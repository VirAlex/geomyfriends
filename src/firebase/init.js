import firebase from "firebase";
 import "firebase/firestore";

// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: "AIzaSyB0InlJP4Tq_r-NfgDV1ENZXuttpREPnzU",
  authDomain: "geofriendsalex.firebaseapp.com",
  databaseURL: "https://geofriendsalex.firebaseio.com",
  projectId: "geofriendsalex",
  storageBucket: "geofriendsalex.appspot.com",
  messagingSenderId: "53349404809",
  appId: "1:53349404809:web:fab859be87dec7f2763658"
};
// Initialize Firebase
const firebaseApp = firebase.initializeApp(firebaseConfig);

export default firebaseApp.firestore();
