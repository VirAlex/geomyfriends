import Vue from 'vue'
import Router from 'vue-router'
import GMap from '@/components/home/GMap'
import Signup from "@/components/auth/Signup";
import Login from "@/components/auth/Login";
import Profile from "@/components/profile/Profile";
import firebase from "firebase"

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: "/",
      name: "GMap",
      component: GMap,
      meta: {
        requiresAuth: true,
      }
    },
    {
      path: "/signup",
      name: "Signup",
      component: Signup,
    },
    {
      path: "/login",
      name: "Login",
      component: Login
    },
    {
      path: "/profile/:id",
      name: "Profile",
      component: Profile,
      meta: {
        requiresAuth: true,
      }
    }
  ]
});

router.beforeEach((to, from, next)=> {
  //check if route = auth
  if(to.matched.some(rec=>rec.meta.requiresAuth)){
    // check auth state user
    let user = firebase.auth().currentUser
    if(user){
      //user signed,in
      next()
    } else {
      // no user signed in, redirect
      next({name: 'Login' })
    }
  } else {
    next()
  }
})


export default router;
